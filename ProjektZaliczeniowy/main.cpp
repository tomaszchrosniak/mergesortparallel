#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <vector>
#include <string>
#include <omp.h>
#include <cinttypes>
#include <cerrno>

using namespace std;

void sortAndMerge(vector<int>*v1, vector<int>*v2, vector<int>*outputVector)
{
	(*outputVector).clear();
	int v1Size = (*v1).size();
	int v2Size = (*v2).size();
	for (int i = 0; i < v1Size; ++i)
	{
		for (int j = 0; j <v2Size; ++j)
		if ((*v1)[i] < (*v2)[j])
			{
			(*outputVector).push_back((*v1)[i]);
				break;
			}
			else
			{
				(*outputVector).push_back((*v2)[j]);
				(*v2).erase((*v2).begin() + j);
				v2Size = (*v2).size();
				j--;
			}
		if (v2Size == 0)
			(*outputVector).push_back((*v1)[i]);
	}
	for (int j = 0; j < v2Size; ++j)
		(*outputVector).push_back((*v2)[j]);
}

void breakApart(vector<int>*inputVector, vector<vector<int>>*unorderedItems)
{
	for (int i = 0; i < inputVector->size(); ++i)
	{
		unorderedItems->push_back(vector<int>(1, (*inputVector)[i]));
	}
}

vector<int> inputData()
{
	int length;
	string lengthStr;
	do{
		cout << "Input the initial vector length:" << endl;
		cin >> lengthStr;
		length = strtoimax(lengthStr.c_str(), nullptr, 10);
	} while (errno==ERANGE || length<2 || length == INTMAX_MAX);
	vector<int> initialVector;
	for (int i = 0; i < length; ++i)
	{
		initialVector.push_back((int)(rand()));
#ifdef __DEBUG__
		cout << initialVector[i] << ",";
#endif
	}
	return(initialVector);
}

int main()
{
		do{
#ifdef __DEBUG__
			cout << "Debug mode on." << endl;
#endif
			double begin_t, end_t, omp_t;
			
			vector<int> inputVector = inputData();
			vector<vector<int>> unorderedItems(inputVector.size());
			breakApart(&inputVector, &unorderedItems);

			int numberOfActiveThreads, startingVectorLength = unorderedItems.size();
			cout << endl << "Number of available threads:" << omp_get_max_threads() << endl;
#ifdef __DEBUG__
			string numberOfActiveThreadsStr;
			do{
				cout << "Input the number of threads that you want to use:" << endl;
				cin >> numberOfActiveThreadsStr;
				numberOfActiveThreads = strtoimax(numberOfActiveThreadsStr.c_str(), nullptr, 10);
			} while (errno == ERANGE || (numberOfActiveThreads > omp_get_max_threads()) || (numberOfActiveThreads <= 0));
			cout << "Vector length: " << unorderedItems.size() << endl;
#endif
#ifndef __DEBUG__
			numberOfActiveThreads = omp_get_max_threads();
#endif
			begin_t = omp_get_wtime();
			while (unorderedItems.size() > 1)
			{
				vector<vector<int>> temporaryBuffer(((unorderedItems.size() - (unorderedItems.size() % 2)) / 2));
#ifdef __DEBUG__
				cout << "New iteration." << endl;
#endif
#pragma omp parallel if(((unorderedItems.size() - (unorderedItems.size() % 2)) / 2)>=numberOfActiveThreads && startingVectorLength>1200) default(none) shared(temporaryBuffer, numberOfActiveThreads, unorderedItems) num_threads(numberOfActiveThreads)
				{
#ifdef __DEBUG__
					int threadNumber = omp_get_thread_num();
					printf("Thread number: %d / %d \n", (threadNumber + 1), omp_get_num_threads());
#endif
#pragma omp for
					for (int i = 0; i < ((unorderedItems.size() - (unorderedItems.size() % 2)) / 2); ++i)
					{
#ifdef __DEBUG__
						printf("[%d:] Current iteration number: %d / %d \n", threadNumber, (i + 1), ((unorderedItems.size() - (unorderedItems.size() % 2)) / 2));
#endif
						sortAndMerge(&(unorderedItems[2*i]), &(unorderedItems[(2*i) + 1]),&(temporaryBuffer[i]));
					}
				}
#ifdef __DEBUG__
				cout << "Threads synchronized. Number of vectors post-merge on the current level: " << ((temporaryBuffer).size() + (unorderedItems.size() % 2)) << endl;
#endif
				if (unorderedItems.size() % 2 != 0)
					temporaryBuffer.push_back(unorderedItems[unorderedItems.size() - 1]);
				unorderedItems = temporaryBuffer;
#ifdef __DEBUG__
				printf("\n");
#endif
			}
			end_t = omp_get_wtime();
			omp_t = end_t - begin_t;

#ifdef __DEBUG__
			cout << "Sorted items:";
			for (int i = 0; i < unorderedItems[0].size(); ++i)
			{
				cout << unorderedItems[0][i] << ",";
			}
			cout << "Vector length: " << unorderedItems[0].size() << endl;
#endif
			cout << "Total computation time for " << numberOfActiveThreads << " active threads is: " << omp_t << endl;
			cout << "Press [Enter], to retry or 'x' to exit." << endl;
			getchar();
		} while (getchar() != 'x');

		getchar();

	return 0;
}
